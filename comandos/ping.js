module.exports = {
    config: {
        name: "ping",
        desc: "Mostra o ping do bot",
        aliases: ["latência"],
        use: "prefixping",
    },
    run: async (client, message, args, Discord) => {
        const m = await message.channel.send("Pong!")
        const ping = m.createdTimestamp - message.createdTimestamp
        const api = Math.floor(parseInt(client.ping))
        message.channel.send(`Tenho ${ping} ms de ping e ${api} ms na API`)
    }
}